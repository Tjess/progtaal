package nl.han.ica.icss.checker;

import java.util.*;
import java.util.stream.Collectors;


import com.google.errorprone.annotations.Var;
import nl.han.ica.icss.ast.*;
import nl.han.ica.icss.ast.literals.*;
import nl.han.ica.icss.ast.operations.AddOperation;
import nl.han.ica.icss.ast.operations.MultiplyOperation;
import nl.han.ica.icss.ast.operations.SubtractOperation;
import nl.han.ica.icss.ast.types.ExpressionType;


public class Checker {

    // Met dank aan http://github.com/hyhyn/ICSSTool/main/java/nl/han/ica/icss/checker/Checker.java

        //private LinkedList<HashMap<String, ExpressionType>> variableTypes;
        private LinkedList<HashMap<String, Expression>> exprHashmap;

        public void check(AST ast) {

           // variableTypes = new LinkedList<>();
            exprHashmap = new LinkedList<>();
           addExpressionTypesToList(ast.root.getChildren(),0);
        }
public void addExpressionTypesToList(List<ASTNode> children, int level) {
    int currentLevel = level;
    exprHashmap.addFirst(new HashMap<>());
    exprHashmap.peekFirst().put(Integer.toString(level), null);
    List<ASTNode> stylerules = new ArrayList<>();

    for (ASTNode node : children) {
        if (node instanceof VariableAssignment) {
            exprHashmap.peekFirst().put(((VariableAssignment) node).name.name, ((VariableAssignment) node).expression);
        } else if (node instanceof Declaration) {
            exprHashmap.peekFirst().put(((Declaration) node).property.name, ((Declaration) node).expression);
            if (node.getChildren().get(1) instanceof VariableReference) {
                if (!checkVariableExistence((VariableReference) node.getChildren().get(1))) {
                    node.setError("ongedefineerde Variabele");
                }
            }
            if (node.getChildren().get(1) instanceof Operation) {
                checkOperationType(node.getChildren().get(1));
            }
            checkDeclaration(node);

        } else if (node instanceof Stylerule) {
            stylerules.add(node);
            addExpressionsToList(node.getChildren(), level);
        }
        // Github geeft exprHashMap.RemoveFirst(); wat fout gaat
        // geeft vervolgens level++; Wat zou moeten werken

        level++;
    }
}

    private void addExpressionsToList(ArrayList<ASTNode> children, int level) {
            int currentLevel = level;
            exprHashmap.addFirst(new HashMap<>());
            exprHashmap.peekFirst().put(Integer.toString(level), null);
            List<ASTNode> stylerules = new ArrayList<>();

            for (ASTNode node : children){
                if(node instanceof VariableAssignment) {
                    exprHashmap.peekFirst().put(((VariableAssignment) node).name.name,((VariableAssignment) node).expression);
                }
                //else
                    if (node instanceof Declaration) {
                    exprHashmap.peekFirst().put(((Declaration) node).property.name,((Declaration) node).expression);

                    if(node.getChildren().get(1) instanceof VariableReference) {
                        if(!checkVariableExistence((VariableReference) node.getChildren().get(1))){
                            node.setError("ongedefinieerde variabele");
                        }
                    }
                    if (node.getChildren().get(1) instanceof Operation){
                        checkOperationType(node.getChildren().get(1));
                    }
                    checkOperationType(node);
                }
                else if (node instanceof  Stylerule){
                    stylerules.add(node);
                    addExpressionTypesToList(node.getChildren(),level);
                }
               level++;
            }
    }

    private ASTNode checkOperationType(ASTNode operation) {
            if(operation instanceof AddOperation || operation instanceof SubtractOperation){
                checkAddSub(operation);
                checkOperationChildren((Operation)operation);
                System.out.println("Instance of Add/Sub Operation");
            } else if (operation instanceof MultiplyOperation){
                checkMul(operation);
                checkOperationChildren((Operation)operation);
                System.out.println("instance of MultiplyOperation");
            } else return operation;

            return operation;
    }

    private void checkOperationChildren(Operation operation)
    {
        if(operation.lhs instanceof ColorLiteral)
            operation.lhs.setError("Kleur mag niet worden gebruikt in een operation");
        if(operation.rhs instanceof ColorLiteral)
            operation.rhs.setError("Kleur mag niet worden gebruikt in een operation");
    }

    private void checkMul(ASTNode operation) {
            for(ASTNode child : operation.getChildren()){
                if (child instanceof Operation){
                    checkOperationType(child);
                } else if (child instanceof VariableReference) {
                    if(!checkScalarInMap(child)) {
                        child.setError("Variabele is geen SCALAR");
                    }
                }else if (child instanceof Literal) {
                    if (!(child instanceof ScalarLiteral)) {
                        child.setError("Alleen Scalarwaarden mogen gebruikt worden voor vermenigvuldigen");
                    }
                }
            }
    }

    private boolean checkScalarInMap(ASTNode ref) {
            VariableReference varRef = (VariableReference) ref;
            for (HashMap map : exprHashmap) {
                if(map.get(varRef.name) instanceof ScalarLiteral) {
                    return true;
                }
            }
            return false;
    }

    private ExpressionType checkAddSub(ASTNode operation) {
            ExpressionType litType = null;
            for(ASTNode child : operation.getChildren()){
                if(child instanceof Operation){
                    //Checkt het child, dit hoeft niet het type te checken
                    ExpressionType type = checkAddSub(child);
                    if(litType == null){
                        litType = type;
                    }else  if (type == ExpressionType.SCALAR || type == ExpressionType.PIXEL) {
                        // dit mag
                    } else if(litType != type) {
                        child.setError("Plus en Min mogen alleen gebruikt worden voor gelijke types");
                    }
               }else if (child instanceof Literal) {
                    ExpressionType type = checkLiteral(child);
                    if (litType == null) {
                        litType = type;
                    }else if (type ==ExpressionType.SCALAR || type == ExpressionType.PIXEL) {
                        // dit mag
                    } else if (litType != type){
                        child.setError("Plus en Min mogen alleen gebruikt worden voor gelijke types");
                    }
                    }else if (child instanceof VariableReference) {
                    ExpressionType type = checkLiteral(getVariableExistenceInMap(child));
                    if(litType == null) {
                        litType = type;
                    }else if (type == ExpressionType.SCALAR || type == ExpressionType.PIXEL) {
                        // dit mag
                    } else if (litType != type) {
                        child.setError("Plus en Min mogen alleen gebruikt worden voor gelijke types");
                    }
                }
            }
            if (litType == ExpressionType.COLOR) {
                operation.setError("Plus en min operaties kunnen niet gebruikt worden voor kleuren");
            }
            return litType;
    }

    private boolean checkVariableExistence(VariableReference variableReference) {
           for(HashMap map : exprHashmap){
               if(map.get(variableReference.name) != null) {
                   return true;
               }
           }
           return false;
    }

    // Check Declaratie
    private void checkDeclaration(ASTNode node){
            Declaration declaration = (Declaration) node;
            String name = declaration.property.name;
            if(!(declaration.expression instanceof Operation)){
                if(name.equalsIgnoreCase("width") || name.equalsIgnoreCase("height")){
                    if(checkLiteral(getVariableExistenceInMap(declaration.expression))!= ExpressionType.PIXEL){
                        declaration.setError("Declaratie heeft geen geldige waarde");
                    } else if (!(declaration.expression instanceof PixelLiteral)){
                        declaration.setError("Declaratie heeft geen geldige waarde");
                    }
                }else if (name.equalsIgnoreCase("color") || name.equalsIgnoreCase("background-color")){
                    if(declaration.expression instanceof VariableReference) {
                        if(checkLiteral(getVariableExistenceInMap(declaration.expression)) != ExpressionType.COLOR){
                            declaration.setError("Declaratie heeft geen geldige waarde");
                        } else if (!(declaration.expression instanceof ColorLiteral)){
                            declaration.setError("Declaratie heeft geen geldige waarde");
                        }
                    }
                }
            }
    }

    private Literal getVariableExistenceInMap(ASTNode ref) {
        VariableReference varRef = (VariableReference) ref;
        Literal lit;
        for(HashMap map : exprHashmap) {
            lit = (Literal)map.get(((VariableReference) ref).name);
            if(lit != null) {
                return lit;
            }
        }
        return null;
    }

    private ExpressionType checkLiteral (ASTNode reference){
            if(reference instanceof ColorLiteral) {
                return ExpressionType.COLOR;
            } else if(reference instanceof BoolLiteral){
                return ExpressionType.BOOL;
            }else if (reference instanceof PercentageLiteral){
                return ExpressionType.PERCENTAGE;
            }else if (reference instanceof PixelLiteral){
                return ExpressionType.PIXEL;
            }else if (reference instanceof ScalarLiteral){
                return ExpressionType.SCALAR;
            }else return ExpressionType.UNDEFINED;
}


    public static <String, Expression> Set<String> getKeysByValue(Map<String, Expression> map, Expression value){
            return map.entrySet()
                    .stream()
                    .filter(entry -> Objects.equals(entry.getValue(),value))
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toSet());
    }
}






