package nl.han.ica.icss.parser;


import java.util.ArrayList;
import java.util.Stack;
import nl.han.ica.icss.ast.*;
import nl.han.ica.icss.ast.literals.*;
import nl.han.ica.icss.ast.operations.AddOperation;
import nl.han.ica.icss.ast.operations.MultiplyOperation;
import nl.han.ica.icss.ast.operations.SubtractOperation;
import nl.han.ica.icss.ast.selectors.ClassSelector;
import nl.han.ica.icss.ast.selectors.IdSelector;
import nl.han.ica.icss.ast.selectors.TagSelector;



/**
 * This class extracts the ICSS Abstract Syntax Tree from the Antlr Parse tree.
 */
public class ASTListener extends ICSSBaseListener {
	
	//Accumulator attributes:
	private AST ast;

	//Use this to keep track of the parent nodes when recursively traversing the ast
	private Stack<ASTNode> currentContainer;

	public ASTListener() {
		ast = new AST();
		currentContainer = new Stack<>();
	}

	@Override
    public void enterStylesheet(ICSSParser.StylesheetContext ctx) {
	    super.enterStylesheet(ctx);
	    currentContainer.push(new Stylesheet());
    }

	@Override
    public void exitStylesheet(ICSSParser.StylesheetContext ctx) {
        super.exitStylesheet(ctx);
        Stylesheet stylesheet = (Stylesheet)currentContainer.pop();
        ast.setRoot(stylesheet);
    }

    @Override public void enterStylerule(ICSSParser.StyleruleContext ctx) {
        super.enterStylerule(ctx);
        Stylerule stylerule = new Stylerule();
        currentContainer.push(stylerule);
        System.out.println(currentContainer);
    }

    @Override
    public  void exitStylerule(ICSSParser.StyleruleContext ctx){
	    super.exitStylerule(ctx);
        handleUnpoppedEntitiesThenPopCurrentEntity();
//	    ASTNode stylerule = currentContainer.pop();
//	    currentContainer.peek().addChild(stylerule);
    }

    @Override
    public void enterSelector(ICSSParser.SelectorContext ctx) {
	    super.enterSelector(ctx);
	    Selector selector;
	    if(ctx.tagSelector() != null)   selector = new TagSelector(ctx.getText());
	    else if(ctx.idSelector() != null) selector = new IdSelector(ctx.getText());
	    else selector = new ClassSelector(ctx.getText());

	    currentContainer.push(selector);
    }

    @Override
    public void exitSelector(ICSSParser.SelectorContext ctx){
	    super.exitSelector(ctx);
	    ASTNode selector = currentContainer.pop();
	    currentContainer.peek().addChild(selector);
    }

// LITERAL
    @Override
    public void enterLiteral(ICSSParser.LiteralContext ctx) {
        super.enterLiteral(ctx);
	    Literal literal;
	    String name = ctx.getText();
	    if(ctx.boolLiteral() != null) literal = new BoolLiteral(name);
	    else if (ctx.colorLiteral() != null) literal = new ColorLiteral(name);
	    else if (ctx.pixelLiteral() != null) literal = new PixelLiteral(name);
	    else if(ctx.scalarLiteral() != null) literal = new ScalarLiteral(name);
	    else literal = new PercentageLiteral(name);

	    currentContainer.add(literal);
    }

    @Override
    public void exitLiteral(ICSSParser.LiteralContext ctx){
	    super.exitLiteral(ctx);
	    ASTNode literal = currentContainer.pop();
	    evaluateParentAndThenPlaceExpressionInAST((Literal)literal);
	    //currentContainer.peek().addChild(literal);
    }

    @Override
    public void enterVariable(ICSSParser.VariableContext ctx){
	    super.enterVariable(ctx);
	    VariableAssignment variableAssignment = new VariableAssignment();
	    currentContainer.push(variableAssignment);
    }

    @Override
    public void exitVariable(ICSSParser.VariableContext ctx){
	    super.exitVariable(ctx);
	    handleUnpoppedEntitiesThenPopCurrentEntity();
//	    ASTNode variableAssignment = currentContainer.pop();
//	    currentContainer.peek().addChild(variableAssignment);
    }

    @Override
    public void enterVariableReference(ICSSParser.VariableReferenceContext ctx){
	    super.enterVariableReference(ctx);
	    VariableReference variableReference = new VariableReference(ctx.getText());
	    currentContainer.push(variableReference);
    }

    @Override
    public void exitVariableReference(ICSSParser.VariableReferenceContext ctx) {
        super.exitVariableReference(ctx);
        ASTNode variableReference = currentContainer.pop();
        evaluateParentAndThenPlaceExpressionInAST((Expression)variableReference);
//        currentContainer.peek().addChild(variableReference);
    }

    @Override
    public void enterDeclaration(ICSSParser.DeclarationContext ctx){
	    super.enterDeclaration(ctx);
	    Declaration declaration = new Declaration(ctx.getText());
	    currentContainer.push(declaration);
    }

    @Override
    public void exitDeclaration(ICSSParser.DeclarationContext ctx){
	    super.exitDeclaration(ctx);
	    handleUnpoppedEntitiesThenPopCurrentEntity();
//	    ASTNode declaration = currentContainer.pop();
//	    currentContainer.peek().addChild(declaration);
    }

    @Override
    public void enterPropertyName(ICSSParser.PropertyNameContext ctx){
	    super.enterPropertyName(ctx);
	    PropertyName propertyName = new PropertyName(ctx.getText());
	    currentContainer.push(propertyName);
    }

    @Override
    public void exitPropertyName(ICSSParser.PropertyNameContext ctx){
	    super.exitPropertyName(ctx);
	    popNodeAndAddToParent();
//	    ASTNode propertyName = currentContainer.pop();
//	    currentContainer.peek().addChild(propertyName);
    }

    //@Override
//    public void enterOperation(ICSSParser.OperationContext ctx){
//	    super.enterOperation(ctx);
//	    Operation operation;
//	    if(ctx.plusMinOperation() != null){
//	        if(ctx.getText().equals("-"))
//	            operation = new SubtractOperation();
//	        else operation = new AddOperation();
//        }
//        else operation = new MultiplyOperation();
//
//        currentContainer.push(operation);
//    }

    @Override
    public void enterMulOperation(ICSSParser.MulOperationContext ctx)
    {
        super.enterMulOperation(ctx);
        MultiplyOperation multiplyOperation = new MultiplyOperation();

        currentContainer.push(multiplyOperation);
    }

    @Override
    public void exitMulOperation(ICSSParser.MulOperationContext ctx)
    {
        super.exitMulOperation(ctx);
        Operation operation = (Operation)currentContainer.pop();
        handleExitOperation(operation);
    }

    @Override
    public void enterPlusMinOperation(ICSSParser.PlusMinOperationContext ctx)
    {
        super.enterPlusMinOperation(ctx);
        Operation operation;

        if(ctx.getText().equals("-"))
            operation = new SubtractOperation();
        else operation = new AddOperation();

        currentContainer.push(operation);
    }

    @Override
    public void exitPlusMinOperation(ICSSParser.PlusMinOperationContext ctx)
    {
        super.exitPlusMinOperation(ctx);
        Operation operation = (Operation)currentContainer.pop();
        handleExitOperation(operation);
    }

//    @Override
//    public void exitOperation(ICSSParser.OperationContext ctx) {
//	    super.exitOperation(ctx);
//	    ASTNode operation = currentContainer.pop();
//	    handleExitOperation((Operation)operation);
////	    currentContainer.peek().addChild(operation);
//
////        Operation operation;
////        String name = ctx.getText();
////        if(ctx.literal() != null)  operation = new AddOperation();
////        else if(ctx.variableReference() != null) operation = new MultiplyOperation();
////        else operation = new SubtractOperation();
////
////        currentContainer.add(operation);
//
//    }

    @Override
    public void enterIfClause(ICSSParser.IfClauseContext ctx)
    {
        super.enterIfClause(ctx);
        IfClause ifClause = new IfClause();
        currentContainer.push(ifClause);
    }

    @Override
    public void exitIfClause(ICSSParser.IfClauseContext ctx)
    {
        super.exitIfClause(ctx);
        popNodeAndAddToParent();
    }

// In samenwerking met Charlotte, zij heeft mij enorm geholpen met dit stukje.
    private void handleExitOperation(Operation operation)
    {
        ASTNode nextNode = currentContainer.pop();

        // If the next node is a Literal or a VariableReference, we pop that node and add it as a child to this operation.
        if(nextNode instanceof Literal || nextNode instanceof VariableReference)
        {
            operation.addChild(nextNode);
        }
        // If the next node is an add/subtract operation with 2 children, and the current node is a MultiplyOperation, it becomes the right child of
        // the next node and it takes over the right child of that next node as its own child. Both operations stay on the stack.
        else if(nodeIsAddOrSubtractOperationWithTwoChildren(nextNode) && operation instanceof MultiplyOperation)
        {
            operation.addChild(((Operation)nextNode).rhs);
            ((Operation)nextNode).rhs = null;
            currentContainer.push(nextNode);
        }
        // If the next node is an add/subtract operation with 2 children, and the current node is an add/subtract operation, the next node becomes a child
        // of the current node. The next node stays off the stack, while the current node get pushed on the stack.
        else if(nodeIsAddOrSubtractOperationWithTwoChildren(nextNode) && (operation instanceof AddOperation || operation instanceof SubtractOperation))
        {
            operation.addChild(nextNode);
        }
        // If the next node is a MultiplyOperation with two children, we need to evaluate the next node after that as well.
        else if(nodeIsMultiplyOperationWithTwoChildren(nextNode)){
            ASTNode newNextNode = currentContainer.pop();
            // If the new next node is an operation, it takes the next node as its child, and the current node becomes the parent of the
            // new next node.
            if(newNextNode instanceof Operation){
                newNextNode.addChild(nextNode);
                operation.addChild(newNextNode);
            }
            // If the new next node is not an operation, we can't do anything with it yet, so we put it back on the stack. The current
            // node becomes the parent of the next node.
            else {
                currentContainer.push(newNextNode);
                operation.addChild(nextNode);
            }
        }
        // If the next node is a VariableAssignment, the child of the operation must be stored in the expression of the VariableAssignment.
        // We retrieve it, make it the child of the operation instead, and put the VariableAssignment and the current operation back on the
        // stack for later.
        else if(nextNode instanceof VariableAssignment)
        {
            Expression expression = ((VariableAssignment) nextNode).expression;
            operation.addChild(expression);
            ((VariableAssignment) nextNode).expression = null;
            currentContainer.push(nextNode);
        }
        // The operation needs to stay on the stack for a while. It will either be cleaned up when handleExitOperation is called again, or
        // when handleUnpoppedEntitiesThenPopCurrentEntity is called (a general clean-up method).
        currentContainer.push(operation);
    }

    private void popNodeAndAddToParent()
    {
        ASTNode node = currentContainer.pop();
        currentContainer.peek().addChild(node);
    }

    private boolean nodeIsAddOrSubtractOperationWithTwoChildren(ASTNode node){
        return (node instanceof AddOperation || node instanceof SubtractOperation) && node.getChildren().size() == 2;
    }

    private boolean nodeIsMultiplyOperationWithTwoChildren(ASTNode node){
        return node instanceof MultiplyOperation && node.getChildren().size() == 2;
    }

    private void evaluateParentAndThenPlaceExpressionInAST(Expression expression)
    {
        if(currentContainer.peek() instanceof VariableAssignment || currentContainer.peek() instanceof IfClause
                || currentContainer.peek() instanceof Operation)
            currentContainer.peek().addChild(expression);
        else currentContainer.push(expression);
    }

    private void handleUnpoppedEntitiesThenPopCurrentEntity()
    {
        ASTNode nextNode = currentContainer.peek();

        if (nextNode instanceof IfClause || nextNode instanceof Expression)
        {
            popNodeAndAddToParent();
            handleUnpoppedEntitiesThenPopCurrentEntity();
        }
        else popNodeAndAddToParent();
    }


    //IF CLAUSE
//    @Override
//    public void exitIfClause(ICSSParser.IfClauseContext ctx) {
//	    IfClause ifClause;
//	    String name = ctx.getText();
//	   if(ctx.IF() == ctx.boolLiteral()) ifClause = new IfClause();
//	   else ifClause = new IfClause();
//
//	   currentContainer.add(ifClause);
//
//
//
//

 //   }

    public AST getAST() {
        return ast;
    }
}
