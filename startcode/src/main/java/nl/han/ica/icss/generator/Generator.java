package nl.han.ica.icss.generator;

import nl.han.ica.icss.ast.*;
import nl.han.ica.icss.ast.literals.*;
import nl.han.ica.icss.ast.selectors.ClassSelector;
import nl.han.ica.icss.ast.selectors.IdSelector;
import nl.han.ica.icss.ast.selectors.TagSelector;

public class Generator {

	// Charlotte heeft geholpen

	public String generate(AST ast) {
		StringBuilder stringBuilder = new StringBuilder();
		for (ASTNode child : ast.root.getChildren()){
			if(child instanceof Stylerule)
				stringBuilder.append(styleRuleToString((Stylerule)child));
		}
		return stringBuilder.toString();
	}

	private String styleRuleToString(Stylerule stylerule)
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(selectorsToString(stylerule) + " {\n");

		for(ASTNode child : stylerule.body){
			if(child instanceof Declaration){
				stringBuilder.append("  " + ((Declaration) child).property.name + ": " + getLiteralValue(((Declaration)child).expression) + ";\n" );
			}
		}
		stringBuilder.append("}\n");
		return stringBuilder.toString();
	}

	private String selectorsToString(Stylerule stylerule){
		StringBuilder stringBuilder = new StringBuilder();

		for(Selector selector: stylerule.selectors)
		{
			if(selector instanceof TagSelector){
				stringBuilder.append(((TagSelector)selector).tag);
				if(stylerule.selectors.size() > stylerule.selectors.indexOf(selector) + 1)
					stringBuilder.append(", ");
			}
			else if(selector instanceof ClassSelector){
				stringBuilder.append(((ClassSelector)selector).cls);
				if(stylerule.selectors.size() > stylerule.selectors.indexOf(selector) + 1)
					stringBuilder.append(", ");
			}
			else if(selector instanceof IdSelector){
				stringBuilder.append(((IdSelector)selector).id);
				if(stylerule.selectors.size() > stylerule.selectors.indexOf(selector) + 1)
					stringBuilder.append(", ");
			}
		}

		return stringBuilder.toString();
	}

	private String getLiteralValue(Expression expression)
	{
		if(expression instanceof BoolLiteral)
			return ((BoolLiteral) expression).value ? "true" : "false";
		else if(expression instanceof ColorLiteral)
			return ((ColorLiteral) expression).value;
		else if(expression instanceof PercentageLiteral)
			return ((PercentageLiteral) expression).value + "%";
		else if(expression instanceof PixelLiteral)
			return ((PixelLiteral) expression).value + "px";
		else if(expression instanceof ScalarLiteral)
			return Integer.toString(((ScalarLiteral) expression).value);

		return null;
	}
}
