grammar ICSS;

//--- LEXER: ---
// IF support:
IF: 'if';
BOX_BRACKET_OPEN: '[';
BOX_BRACKET_CLOSE: ']';


//Literals
TRUE: 'TRUE';
FALSE: 'FALSE';
PIXELSIZE: [0-9]+ 'px';
PERCENTAGE: [0-9]+ '%';
SCALAR: [0-9]+;

//Color value takes precedence over id idents
COLOR: '#' [0-9a-f] [0-9a-f] [0-9a-f] [0-9a-f] [0-9a-f] [0-9a-f];

//Specific identifiers for id's and css classes
ID_IDENT: '#' [a-z0-9\-]+;
CLASS_IDENT: '.' [a-z0-9\-]+;

//General identifiers
LOWER_IDENT: [a-z] [a-z0-9\-]*;
CAPITAL_IDENT: [A-Z] [A-Za-z0-9_]*;

//All whitespace is skipped
WS: [ \t\r\n]+ -> skip;

//
OPEN_BRACE: '{';
CLOSE_BRACE: '}';
SEMICOLON: ';';
COLON: ':';
PLUS: '+';
MIN: '-';
MUL: '*';
ASSIGNMENT_OPERATOR: ':=';

//--- PARSER: ---

// stylesheet: EOF;

stylesheet: (stylerule|variable)* EOF;

stylerule: selector OPEN_BRACE (declaration | variable | ifClause)* CLOSE_BRACE;

selector: tagSelector | idSelector | classSelector;
tagSelector: LOWER_IDENT;
idSelector: ID_IDENT;
classSelector: CLASS_IDENT;

declaration: propertyName COLON (literal | variableReference | operation) SEMICOLON;
propertyName: LOWER_IDENT;

literal: pixelLiteral
       | colorLiteral
       | boolLiteral
       | scalarLiteral
       | percentageLiteral;
pixelLiteral: PIXELSIZE;
colorLiteral: COLOR;
boolLiteral: TRUE | FALSE;
scalarLiteral: SCALAR;
percentageLiteral: PERCENTAGE;

variable: variableReference variableAssignment;
variableReference: CAPITAL_IDENT;
variableAssignment: ASSIGNMENT_OPERATOR (literal | variableReference | operation) SEMICOLON;

plusMinOperation: PLUS | MIN;
mulOperation: MUL;

operationBaseCase: literal | variableReference;
operation: operationBaseCase
         | operation mulOperation operation
         | operation plusMinOperation operation;

ifClause: IF BOX_BRACKET_OPEN (variableReference | boolLiteral) BOX_BRACKET_CLOSE OPEN_BRACE (declaration | variable | ifClause)* CLOSE_BRACE;


//stylesheet: (stylerule|variable)*;
//
//stylerule: selector OPEN_BRACE declaration* CLOSE_BRACE;
//
//selector: tagSelector | idSelector | classSelector;
//tagSelector: LOWER_IDENT;
//idSelector: ID_IDENT;
//classSelector: CLASS_IDENT;
//
//declaration: propertyName COLON literal SEMICOLON;
//propertyName: LOWER_IDENT;
//
//literal: pixelLiteral |
//         colorLiteral |
//         boolLiteral |
//         scalarLiteral;
//pixelLiteral: PIXELSIZE | variableReference;
//colorLiteral: COLOR| variableReference;
//boolLiteral: TRUE | FALSE | variableReference;
//scalarLiteral: SCALAR | variableReference;
//
//variable: variableReference variableAssignment;
//variableReference: CAPITAL_IDENT;
//variableAssignment: ASSIGNMENT_OPERATOR literal SEMICOLON;
//
//plusMinOperation: PLUS | MIN;
//mulOperation: MUL;
//
//operation: literal
//         | operation mulOperation operation
//         | operation plusMinOperation operation;





